package ru.nechta.test_ci;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
MainActivity m=new MainActivity();
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void max0_isCorrect() {
        assertEquals(m.max(0,-12),0);
    }

    @Test
    public void max1_isCorrect() {
        assertEquals(m.max(4,2),4);
    }

    @Test
    public void max2_isCorrect() {
        assertEquals(m.max(2,-2),2);
    }

    @Test
    public void max3_isCorrect() {
        assertEquals(m.max(-2,-2),-2);
    }

    @Test
    public void max4_isCorrect() {
        assertEquals(m.max(-12,-2),-2);
    }
    @Test
    public void max5_isCorrect() {
        assertEquals(m.max(-2,-12),-2);
    }

}